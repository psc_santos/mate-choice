# Mate Choice Scripts

This repository contains Python and R scripts used for testing mate choice in the following articles: [`Santos et al., 2016`](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5150237/), [`Santos et al., 2017`](https://www.ncbi.nlm.nih.gov/pubmed/28141891) and [`Santos et al., 2018`](https://www.ncbi.nlm.nih.gov/pubmed/30963892).

- The file [`randomizations.py`](randomizations.py) contains the Python3 scripts for Monte Carlo simulations.
- The file [`modeling.r`](modeling.r) contains the R scripts for statistical modelling and effects plotting.
- Both files above can be tested using the dummy dataset (with anonymized IDs, drawn out of a raccoon sample) given in the file [`matechoice.csv`](matechoice.csv) as input.

You are free to copy/clone and change the scripts to suit your own project, by citing one of the papers above.
