setwd("/mnt/HDD2/git/matechoice/")
raccoons = read.csv(file = "matechoice.csv", header = TRUE)

library(lme4)
library(mgcv)
library(ggplot2)
library(effects)
library(sjPlot)
library(sjmisc)

#plot all columns of dataframe in order to see what is worth testing
Map(function(x,y) plot(x, main = y), raccoons, names(raccoons))

#1. does female choice depend on the MHC_I?
ggplot(raccoons, aes(MHC_I_MAADiv, CHOSEN)) + stat_smooth(alpha = 0.3, method = "glm") + geom_point(position=position_jitter(height=.05, width = 0.5), alpha = 0.3) + labs(title = "MHC_I", x = "MAADiv", y = "Probability to be chosen")
******# apparently significant effect towards MORE diversity here. But this graph depicts a GLM only

GLMM1 <- glmer(CHOSEN ~ MHC_I_CALDis + MHC_I_MALDis + MHC_I_uAADis + MHC_I_MAADiv + (1|MOTHER)+(1|FATHER),family=binomial("logit"), data = raccoons)
summary(GLMM1)
#
# if I remove one expl variable at a time and refit the model, the only model that remains explanatory is the following, only with MAADiv:
GLMM2 <- glmer(CHOSEN ~ MHC_I_MAADiv + (1|MOTHER)+(1|FATHER),family=binomial("cloglog"), data = raccoons)
summary(GLMM2)
#

cc <- confint(GLMM2,parm="beta_",method="Wald", level = 0.95) # Fixed-effect coefficients
ctab <- cbind(est=fixef(GLMM2),cc) # confidence intervals, log-odds scale
rtab <- exp(ctab) #Exponentiate to get the odds ratios
print(rtab,digits=3)

plot_model(GLMM2)
plot_model(GLMM2, transform = "plogis")
plot_model(GLMM2, show.values = TRUE, value.offset = .03, vline.color = "red") #****

set_theme(theme = "forest",
          geom.label.size = 3,
          axis.textsize = .9,
          axis.title.size = .9)

plot_model(GLMM2, type = "eff", show.ci = TRUE)

GLMM3 <- glmer(CHOSEN ~ MHC_I_MAADiv:MHC_II_MAADiv + (1|MOTHER)+(1|FATHER),family=binomial(link = "cloglog"), data = raccoons)
summary(GLMM3)
cc <- confint(GLMM3,parm="beta_",method="Wald", level = 0.95) # Fixed-effect coefficients
ctab <- cbind(est=fixef(GLMM3),cc) # confidence intervals, log-odds scale
rtab <- exp(ctab) #Exponentiate to get odds ratios

plot_model(GLMM3)

# plot interaction!
plot_model(GLMM3, type = "int", show.ci = TRUE)
plot_model(GLMM3, type = "int", show.ci = TRUE, mdrt.values = "quart", facet.grid = TRUE)
plot_model(GLMM3, show.values = TRUE, value.offset = .05)
eff3 <- allEffects(GLMM3, xlevels=list(MHC_I_MAADiv=5), confidence.level = 0.8)
plot(eff3, multiline=FALSE, ylab="Probability for a male to be chosen", rug=FALSE)
