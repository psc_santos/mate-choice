import random
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

os.chdir("/mnt/HDD2/git/matechoice/")

# covert matecoice csv file into a pandas dataframe:
df = pd.read_csv("matechoice.csv", sep=",", header=0)

# defines the simulation function
def run_sim(df, metric, nsims=10000):
    cleandf = df[df[metric].notnull()] # first filter NAs out of the df, for that metric
    for elem in set(list(cleandf["OFFSPRING"])):
        offspring_df = cleandf[cleandf["OFFSPRING"] == elem]
        if 1 not in list(offspring_df["CHOSEN"]): # filters out offspring without one chosen dad
            cleandf = cleandf[cleandf["OFFSPRING"] != elem]
        else:
            if len(offspring_df) < 2: # offspring needs at least 1 extra candidate dad
                cleandf = cleandf[cleandf["OFFSPRING"] != elem]
    realdists = []
    for elem in set(list(cleandf["OFFSPRING"])):
        realdist = list(cleandf[(cleandf["OFFSPRING"] == elem) & (cleandf["CHOSEN"] == 1)][metric])[0]
        realdists.append(realdist)
    realmean = np.mean(realdists)
    mean_dists = []
    for i in range(nsims):
        dists = []
        for elem in set(list(cleandf["OFFSPRING"])):
            dists.append(random.choice(list(cleandf[cleandf["OFFSPRING"] == elem][metric])))
        mean_dists.append(np.mean(dists))
    mean_dists = np.array(mean_dists)
    p_1tailed = 1 - (float(sum(abs(realmean) >= abs(mean_dists))) / len(mean_dists))
    p_2tailed = 1 - (float(sum(abs(realmean - np.mean(mean_dists)) >= abs(mean_dists - np.mean(mean_dists))))/len(mean_dists))
    return realmean, mean_dists, p_1tailed, p_2tailed


# runs the simulations and plots the results with seaborn
# this examples concerns MHC_I_MALDis only
realmean, mean_dists, p1, p2 = run_sim(df, "MHC_I_MALDis")
sns.set()
matechoiceplot = sns.distplot(mean_dists, bins=20)
matechoiceplot.set(xlabel="MHC_I_MALDis")
matechoiceplot.vlines(realmean, 0, 3, colors="black", linestyles="--")
text = "p = " + str(round(p2, 5))
drift = 0.05 * (mean_dists.max() - mean_dists.min())
matechoiceplot.text(realmean+drift, 2, text)

fig = matechoiceplot.get_figure()
fig.savefig("MHC_I_MALDis.png")
